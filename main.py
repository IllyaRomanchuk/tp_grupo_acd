from veiculos import (cria_novo_veiculo,
                          imprime_lista_de_veiculos,
                          nome_ficheiro_lista_de_veiculos,
                          descricao_veiculo,
                          veiculos_descricao,
                          imprime_descricao,
                          preco_dos_veiculos,
                          imprime_precos,
                          preco_veiculos
                          )
from utilizadores import (cria_novo_utilizador,
                              imprime_lista_de_utilizadores,
                              nome_ficheiro_lista_de_utilizadores,
                              nome_ficheiro_lista_de_compras,
                              imprime_lista_de_compras,
                              )
from io_ficheiros import (carrega_as_listas_dos_ficheiros,
                              guarda_as_listas_em_ficheiros
                              )
from io_terminal import pergunta_id
from noticias import (mostrar_noticias,
                          mostrar_eventos)


def menu():
    """ Main menu da aplicação
        Aqui neste menu iremos ter todas as funções necessárias para trabalhar com o nossa aplicação.

    """

    lista_de_veiculos = []
    lista_de_utilizadores = []
    lista_des_veiculos = []
    lista_de_compras = []
    preco_veiculo = []

    while True:
        print("\033[1;95m" + """
        =====================================================================
        |             MOTO IA - O STAND MAIS BARATO DE PORTUGAL             |
        =====================================================================
        | nt - Noticias em destaque     ev - Eventos e concentrações        |
        | vn - Novo veiculo             vl - Lista de veiculos              |
        |  ad - Adicionar Descriçao       des - Ver Descrições              |
        | un - Novo utilizador          ul - Lista de utilizadores          |
        | cn - Nova compra              cl - Lista de compras               |
        =====================================================================
        | g - Guardar tudo             c - Carregar dados salvos            |
        | x - Sair                                                          |
        =====================================================================
        """ + "\033[0;0m")

        op = input("\033[1;95m"+"O que pretende escolher? "+"\033[0;0m").lower()

        if op == "x":
            exit()
        elif op == "vn":
            novo_veiculo = cria_novo_veiculo()
            preco = preco_dos_veiculos()
            lista_de_veiculos.append(novo_veiculo)
            preco_veiculo.append(preco)
        elif op == "ad":
            des_veiculo = pergunta_id(questao="Qual o id do veiculo que quer adicionar descrição?", lista=lista_de_veiculos), descricao_veiculo()
            lista_des_veiculos.append(des_veiculo)
        elif op == "vl":
            imprime_lista_de_veiculos(lista_de_veiculos)
            imprime_precos(preco_veiculo)
        elif op == "un":
            novo_utilizador = cria_novo_utilizador()
            lista_de_utilizadores.append(novo_utilizador)
        elif op == "ul":
            imprime_lista_de_utilizadores(lista_de_utilizadores)
        elif op == "des":
            imprime_descricao(lista_des_veiculos)
        elif op == "g":
            guarda_as_listas_em_ficheiros(lista_de_veiculos,
                                          lista_de_utilizadores,
                                          lista_de_compras,
                                          lista_des_veiculos,
                                          preco_veiculo,
                                          nome_ficheiro_lista_de_veiculos,
                                          nome_ficheiro_lista_de_utilizadores,
                                          nome_ficheiro_lista_de_compras,
                                          veiculos_descricao,
                                          preco_veiculos,
                                          )
        elif op == "c":
            lista_de_veiculos, lista_de_utilizadores, lista_de_compras, lista_des_veiculos, preco_veiculo = carrega_as_listas_dos_ficheiros(
                nome_ficheiro_lista_de_veiculos=nome_ficheiro_lista_de_veiculos,
                nome_ficheiro_lista_de_utilizadores=nome_ficheiro_lista_de_utilizadores,
                nome_ficheiro_lista_de_compras=nome_ficheiro_lista_de_compras,
                veiculos_descricao=veiculos_descricao,
                preco_veiculos=preco_veiculos,
            )
        elif op == "cn":
            from datetime import date
            today = date.today()
            d1 = today.strftime("%d/%m/%Y")
            id_comprador = pergunta_id(questao="Qual o id do comprador?", lista=lista_de_utilizadores)
            id_veiculo = pergunta_id(questao="Qual o id do veiculo?", lista=lista_de_veiculos)
            id_preco1 = pergunta_id(questao="Introduza o id do veiculo mais uma vez.", lista=preco_veiculo)
            lista_de_compras.append([id_comprador, id_veiculo, id_preco1, d1])
        elif op == "cl":
            imprime_lista_de_compras(lista_de_compras)
        elif op == "nt":
            mostrar_noticias()
        elif op == "ev":
            mostrar_eventos()


print("test")
if __name__ == "__main__":
    menu()
