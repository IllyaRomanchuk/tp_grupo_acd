### Para que serve este repositório? ###

> Neste repositório foi feito um Stand de Motos em python.

### Como faço para configurar? ###

> Para obter essa resposta basta ir na [Wiki.](https://bitbucket.org/IllyaRomanchuk/tp_grupo_acd/wiki/Home)

#### Documentação ####

> Nossa Documentação foi feita através do Sphinx.

### Com quem devo falar? ###

Deve contatar os administradores do repositório:

> Illya Romanchuk

> Afonso Duarte

![Programador](https://media.discordapp.net/attachments/423980044814123010/805524164597121124/ecommerce-marketing-visual-content-visual-marketing-header-wide.gif?width=1100&height=375)
