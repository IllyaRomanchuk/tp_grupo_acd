from io_terminal import imprime_lista

nome_ficheiro_lista_de_veiculos = "lista_de_veiculos.pk"
veiculos_descricao = "descricao_veiculos.pk"
preco_veiculos = "preco_veiculos"


def cria_novo_veiculo():
    """ Pede ao utilizador para introduzir um novo veiculo

    :return: dicionario com um veiculo na forma
        {"marca": <<marca>>, "matricula": <<matricula>>, "modelo": <<modelo>>}
    """

    marca = input("Marca? ")
    matricula = input("Matricula? ").upper()
    modelo = input("Modelo? ")
    return {"Marca": marca, "Matricula": matricula, "Modelo": modelo}


def imprime_lista_de_veiculos(lista_de_veiculos):
    """ Imprime a lista de veiculos disponiveis para compra."""

    imprime_lista(cabecalho="Lista de Veiculos", lista=lista_de_veiculos)


def imprime_descricao(lista_des_veiculos):
    """Imprime a descrição dos veiculos com o respetivo id antes."""

    imprime_lista(cabecalho="Descriçao Do Veiculo", lista=lista_des_veiculos)


def descricao_veiculo():
    """Pede ao utilizador para inserir uma descrição do veiculo."""

    descricao = input("Adicionar descrição do veiculo: ")
    return {"Descrição": descricao}


def preco_dos_veiculos():
    """Nesta função iremos pedir o preço do veiculo."""
    try:
        preco = int(input("Preço? "))
        return {"Preço": preco}
    except ValueError:
        print("\033[1;31m"+"O preço deverá conter apenas numeros, tente novamente."+"\033[0;0m")


def imprime_precos(preco_veiculo):
    """Imprime os preços dos veiculos com o respetivo id antes."""

    imprime_lista(cabecalho="Preços dos Veiculos", lista=preco_veiculo)