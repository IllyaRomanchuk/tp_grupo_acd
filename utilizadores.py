from io_terminal import imprime_lista

nome_ficheiro_lista_de_utilizadores = "lista_de_utilizadores.pk"
nome_ficheiro_lista_de_compras = "lista_de_compras.pk"


def cria_novo_utilizador():
    """ pede os dados de um novo utilizador

    :return: dicionario com o novo utilizador, {"nome": <<nome>>, "email": <<email>>}
    """
    nome = input("Nome: ")
    email = input("Email: ")

    return nome, email


def imprime_lista_de_utilizadores(lista_de_utilizadores):
    """ Imprime todos os utilizadores existens."""

    imprime_lista(cabecalho="Lista de Utilizadores", lista=lista_de_utilizadores)


def imprime_lista_de_compras(lista_de_compras):
    """Imprime a lista de compras dos nossos veiculos(id)."""

    imprime_lista(cabecalho="Lista de Compras", lista=lista_de_compras)