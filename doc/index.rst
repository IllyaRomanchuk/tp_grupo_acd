.. MotoIA documentation master file, created by
   sphinx-quickstart on Sun Jan 31 00:37:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MotoIA's documentation!
==================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   io_ficheiros
   io_terminal
   main
   noticias
   utilizadores
   veiculos
   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
