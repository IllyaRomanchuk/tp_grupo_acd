import pickle


def le_de_ficheiro(nome_ficheiro):
    """ le os dados de um ficheiro

    :param nome_ficheiro: nome do ficheiro onde estao os dados
    :return: o que leu do ficheiro (depende dos dados guardados)
    """

    with open(nome_ficheiro, "rb") as f:
        return pickle.load(f)


def guarda_em_ficheiro(nome_do_ficheiro, dados):
    """
    guarda a informação contida nas listas em ficheiros, fazendo com que os dados sejam
    salvos e possam ser usados na proxima vez que o programa for aberto

    :param nome_do_ficheiro: nome do ficheiro onde vai guardar os dados
    :param dados: dados a serem guardados
    """

    with open(nome_do_ficheiro, "wb") as f:
        pickle.dump(dados, f)


def carrega_as_listas_dos_ficheiros(nome_ficheiro_lista_de_veiculos,
                                    nome_ficheiro_lista_de_utilizadores,
                                    nome_ficheiro_lista_de_compras,
                                    veiculos_descricao,
                                    preco_veiculos):
    """
    Nesta função obrigamos a nossa lista a ler o arquivo correspondente a lista
    Se o programa ja tiver dados nas listas apos a inserção e tentarcarregar os
    dados nos ficheiros (ou seja nenhuns, os dados anteriores irão ser sobrepostos)
    """

    lista_de_veiculos = le_de_ficheiro(nome_ficheiro_lista_de_veiculos)
    lista_de_utilizadores = le_de_ficheiro(nome_ficheiro_lista_de_utilizadores)
    lista_de_compras = le_de_ficheiro(nome_ficheiro_lista_de_compras)
    lista_des_veiculos = le_de_ficheiro(veiculos_descricao)
    preco_veiculo = le_de_ficheiro(preco_veiculos)
    return  lista_de_veiculos, lista_de_utilizadores, lista_de_compras, lista_des_veiculos, preco_veiculo


def guarda_as_listas_em_ficheiros(lista_de_veiculos,
                                  lista_de_utilizadores,
                                  lista_de_compras,
                                  lista_des_veiculos,
                                  preco_veiculo,
                                  nome_ficheiro_lista_de_veiculos,
                                  nome_ficheiro_lista_de_utilizadores,
                                  nome_ficheiro_lista_de_compras,
                                  veiculos_descricao,
                                  preco_veiculos
                                  ):
    """ .......

    :param _lista_de_utilizadores:
    :param _lista_de_veiculos:
    :return:
    """

    op = input("Os dados nos ficheiros serão sobrepostos. Continuar (S/n)?")
    if op in ['s', 'S']:
        guarda_em_ficheiro(nome_ficheiro_lista_de_veiculos, lista_de_veiculos)
        guarda_em_ficheiro(nome_ficheiro_lista_de_utilizadores, lista_de_utilizadores)
        guarda_em_ficheiro(nome_ficheiro_lista_de_compras, lista_de_compras)
        guarda_em_ficheiro(veiculos_descricao, lista_des_veiculos)
        guarda_em_ficheiro(preco_veiculos, preco_veiculo)
    else:
        print("Operação cancelada.")