def mostrar_noticias():
    """
    Aqui as noticias principais do mundo das motas serão apresentadas.
    """
    print("\033[1;36m" + """
                                 .:@@@                                      
                            .:@@@@@@:  ..!@@@@!..             :@@@@@@@@@@@!.
                        .@@@@@@@@!#@@@@@@@@@@@@@@@@          :@@@@@@@@@@@@@.
                     !@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#.    
                    :@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!:          
                    .!@@@@#!@@@@ !@@@@@@@@@@@@@@@@@@@@@@!:.   :!@@@@!!      
                 !@@@#!  .@@@@@!# #@@@@@@@@@@@@@@@@@@@@@:  #@@@!:  .!@@@#   
                @@@!    #@!  @@@!! @@@@@@@@@@@@@@@@@@@@@@@@@@:        .@@#. 
                @@@          :@@#!@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@!   !@@: 
                .#@@#.     :@@@:                          :@@@#      #@@@.  
                   :#@@@@@@@!.                               !@@@@@@@@:.    
                """ + "\033[0;0m")

    print("\033[;1m" + "Triumph Speed Triple 1200 RS: mais potente e mais leve" + "\033[0;0m")
    print("\033[1;36m" + "Nova Hayabusa da Suzuki a caminho em 2021" + "\033[0;0m")
    print("\033[;1m" + "Rumores: Nova Tuareg 660 da Aprilia" + "\033[0;0m")
    print("\033[1;36m" + "Nova Yamaha MT-07 2021" + "\033[0;0m")
    print("\033[;1m" + "Nova geração da Piaggio Beverly em 2021" + "\033[0;0m")
    print("\033[1;36m" + "Ducati mostra nova Monster para 2021" + "\033[0;0m")


def mostrar_eventos():
    """
    Aqui os principais eventos e concentrações relacionadas a motas irão ser apresentados.
    """
    print("\033[1;41m"+"\033[1;97m"+ """                                                 
                                                                  @@@@                   @@@@                                                                           
                                                              .!!!@@@@!!!!!!!!!!!!!!!!!!!@@@@!!!.                                                                       
                                                           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                                                                    
                                                           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                                                                    
                                                           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                                                                    
                                                           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                                                                    
                                                           @@@@:                               :@@@@                                                                    
                                                           @@@@:                       @       :@@@@                                                                    
                                                           @@@@:                    #@@@@      :@@@@                                                                    
                                                           @@@@:                 :@@@@.        :@@@@                                                                    
                                                           @@@@:     @@@@@     @@@@#           :@@@@                                                                    
                                                           @@@@:       .@@@@@@@@@              :@@@@                                                                    
                                                           @@@@:          !@@@                 :@@@@                                                                    
                                                           @@@@:                               :@@@@                                                                    
                                                           @@@@:                               :@@@@                                                                    
                                                           @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@                                                                    
                                                              .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!.                                                                       
                                                                                                                                                                                        \n""" + "\033[0;0m")
    print("\033[;1m" + """
1) Portugal de Lés-a-lés | meados de junho
O nome deste passeio diz tudo, de Albufeira a Vila Pouca de Aguiar este evento que se estende por 3 dias e 1000 km conta com um limite de 1000 inscritos e é organizada 
pela comissão de Mototurismo da Federação de Motociclismo de Portugal. Existem também outros dois paralelos – o Lés-a-Lés Off Road, vertente todo-o-terreno do evento, 
que tem início em Chaves e termina em Faro e o Lés-a-Lés Quad & UTV, uma vertente Off Road desta vez para motociclos de 4 rodas. Qualquer destes eventos não tem um cariz
competitivo e o objetivo é cruzar Portugal de extremo a extremo contemplando paisagens e lugares de enorme esplendor e, claro, o convívio dentro do espírito motard.

2) Faro | meados de julho
Organizada pelo Moto Clube de Faro é, sem dúvida, o maior evento motard do país. Com mais de 22.000 inscritos de toda a Europa, esta festa junta, para além de uma 
variedade imensa de motos personalizadas, as melhores bandas de música mundial, pelo palco já passaram nomes como Billy Idol, Iron Maiden ou Deep Purple. Se pensa 
em lá ir, despache-se, todos os anos a hotelaria esgota por alturas da concentração.

3) IBEROVESPA | meados de julho
A mítica moto Vespa tem fãs por todo o mundo e Portugal não foge à regra. Aqui só entram Vespas, não interessa o ano ou o modelo desde que sejam da famosa marca italiana.
O Vespa Clube de Lisboa organiza este evento que, de ano a ano, vai visitando zonas maravilhosas do nosso país. 
No ano de 2015 foram mais de 200 ‘Vespistas’ que se reuniram na Costa Alentejana.

4) Góis | meados de agosto
Nas margens do rio Ceira as motas ao lado das muitas tendas não enganam, o espírito desta concentração é “tolerância, liberdade, respeito e civismo”, num cenário 
paradisíaco a festa estende-se pelas praias fluviais, os passeios, o palco principal e a tenda eletrónica onde passam os melhores DJ’s mundiais. Um marco nas concentrações Ibéricas.

5) Pai Natal Motard – Ferreira do Alentejo
O verdadeiro espírito motard é exteriorizado no Natal – Por Portugal, muitos clubes e associações motards saem à rua vestidos de Pai Natal e levam alegria,
confraternização e presentes aos que mais necessitam. As crianças de muitas escolas recebem a visita destes motards ‘diferentes’.
Destacamos a Concentração de Ferreira do Alentejo, mas poderíamos falar de muitas outras. Organizada pelo Moto Grupo de Ferreira do Alentejo,
este evento acontece poucos dias antes do Natal e percorre as escolas das freguesias do concelho, terminando num almoço convívio. 

     
     """+ "\033[0;0m")
